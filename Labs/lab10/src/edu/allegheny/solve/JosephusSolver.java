package edu.allegheny.solve;

import java.util.Scanner;
import java.lang.String;
import java.util.Arrays;
import net.datastructures.*;

/**
 * Class which solves Jospehus problem according to two user
 * command line arguments which decide the number of "steps"
 * and number of "children".
 * Utilizes the java.util.Scanner java.lang.String and
 * java.util.Arrays;
 */

public class JosephusSolver {

    private static class StatisticsCalculator {
/* Constructs a new calculateArithmeticMean instance
 * @param long runningTotal
 * @param int
 * @return runningTotal
 * */
    public static long calculateArithmeticMean(Long[] timings, boolean discardFirst) {
      long runningTotal = 0;
      int start = 0;
      if (discardFirst) {
        start = 1;
      }
      for (int i = start; i < timings.length; i++) {
        runningTotal = runningTotal + timings[i];
      }
      return runningTotal / (long)timings.length;
    }
/* Constructs a new calculateVariance instance
 * @param long arithmeticMean
 * @param long runningTotal
 * @param int start
 * @return runningTotal
 */
   public static long calculateVariance(Long[] timings, boolean discardFirst) {
      long arithmeticMean = calculateArithmeticMean(timings, discardFirst);
      long runningTotal = 0;
      int start = 0;
      if (discardFirst) {
        start = 1;
      }
      for (int i = start; i < timings.length; i++) {
        runningTotal = (timings[i] - arithmeticMean) * (timings[i] - arithmeticMean);
      }
      return runningTotal / (long)timings.length;
    }
}

  /** Solution of the Josephus problem using a queue. */
  public static <E> E Josephus(Queue<E> Q, int k) {
    if (Q.isEmpty()) return null;
    while (Q.size() > 1) {
      System.out.println("  Queue: " + Q + "  k = " + k);
      for (int i=0; i < k; i++)
        Q.enqueue(Q.dequeue());  // move the front element to the end
      E e = Q.dequeue(); // remove the front element from the collection
      System.out.println("    " + e + " is out");
      }
    return Q.dequeue();  // the winner
  }

  /** Build a queue from an array of objects */
  public static <E> Queue<E> buildQueue(E a[]) {
    Queue<E> Q = new NodeQueue<E>();
    for (int i=0; i<a.length; i++)
      Q.enqueue(a[i]);
    return Q;
  }

  /** Tester method.
   * @param Num
   * @param numChildren
   * @param Num2
   * @param numSteps
   * @param startTime
   * @param endTime
   * @param timing
   */

  public static void main(String[] args) {
    int i,t, trials =10;
    boolean discardFirst = true;
    Long[] JoTimings = new Long[trials];
    /*
     *Two command line arguments taking in user input.
     */

    Integer Num = new Integer(args[0]); int numChildren = Num.intValue();
    Integer Num2 = new Integer(args[1]); int numSteps = Num2.intValue();

    /*
     * String containing number of "children" or people in this case.
     */

    String[] children = new String[numChildren];

    /*
     * loop which fills array with "people(i+1)".
     */

    for(i=0; i<numChildren; i++){
        String number = String.valueOf(i+1);
        children[i] = "Person"+number;
    }
    /*
     * timing the Josephus method which solves the josephus problem for
     * the current inputs.
     */

    for(t=0; t<trials;t++){
    long startTime = System.currentTimeMillis();
    System.out.println("The winner is " + Josephus(buildQueue(children), numSteps) +"!\n");
    long endTime = System.currentTimeMillis();
    JoTimings[t] = new Long(endTime - startTime);
  }
  System.out.println("\tTiming Results(ms):\n");
  System.out.println(Arrays.toString(JoTimings));
  System.out.println("\tArithmetic Mean (ms): "
            + StatisticsCalculator.calculateArithmeticMean(JoTimings, discardFirst));

}
}
