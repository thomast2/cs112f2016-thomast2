package edu.allegheny.benchmark;

import com.clarkware.Profiler;
import java.io.*;

/**
 *Calculates Fibonacci number for a specified number of trials.
 *Both the Fibonacci number and number of trials are decided by the user.
 *Calculates the average time for each trial.
 */

public class UseFibonacci {

    /**
     *Command-line interface.
     *Main method in UseFibonacci Class.
     *Calls instances of IterativeFibonacci and RecusriveFibonacci Classes.
     *@param args
     *@param i
     *@param Num
     *@param num
     *@param chosen
     *@param choose
     *@param Num2
     *@param trials
     *@param recursiveFib
     *@param recursiveFibLong
     *@param iterativeFib
     *@param IterativeFibonacciLong
     */
    public static void main(String[] args) {

        int i;

        System.out.println("Begin experiment with different Fibonacci " +
                "implementations ..."); System.out.println();


        Integer Num = new Integer(args[0]); int num = Num.intValue();       // Determine the nth Fibonacci number that is going to be calculated

        String chosen = args[1];                                            // determine which algorithm we are supposed to benchmark
        String choose = args[2];                                            // determine which data type we are benchmarking
        Integer Num2 = new Integer(args[3]); int trials = Num2.intValue();  // determine how many trials we will run the experiment for

        if( chosen.equals("recursive") || chosen.equals("all") ) {          //Loop for recursive experiment

            System.out.println("\tRecursive Fibonacci\n\t-------------------");

            if( choose.equals("int") || choose.equals("both") ){            //Loop for recursive int experiment
                for(i=1; i<=trials; i++){
                Profiler.begin("RecursiveFibonacciInt"); int recursiveFib = //RecursiveFibonacci (int) instance
                RecursiveFibonacci.fib(num);
            Profiler.end("RecursiveFibonacciInt");

            System.out.println("(Recursive/int) The " + num + "th Fibonacci " +
                    "number = " + recursiveFib + ".");

            }
            }
            if( choose.equals("long") || choose.equals("both") ){           //Loop for recursive long experiment
                for(i=1; i<=trials; i++){
                Profiler.begin("RecursiveFibonacciLong"); long recursiveFibLong = //RecursiveFibonacci (long) instance
                RecursiveFibonacci.fibLong(num);
            Profiler.end("RecursiveFibonacciLong");

            System.out.println("(Recursive/long) The " + num + "th Fibonacci "
                    + "number = " + recursiveFibLong + ".");

        }
        }
        }


        if( chosen.equals("iterative") || chosen.equals("all") ) {          //Loop for iterative experiment

            System.out.println("\n\tIterative Fibonacci\n\t-------------------");

            if( choose.equals("int") || choose.equals("both") ){            //Loop for iterative int experiment
                for(i=1; i<=trials; i++){
                Profiler.begin("IterativeFibonacciInt"); int iterativeFib = // IterativeFibonacci (int) instance
                IterativeFibonacci.fib(num);
            Profiler.end("IterativeFibonacciInt");

            System.out.println("(Iterative/int) The " + num + "th Fibonacci " +
                    "number = " + iterativeFib + ".");
            }
            }
            if( choose.equals("long") || choose.equals("both") ){           //Loop for iterative long experiment
                for(i=1; i<=trials; i++){
                Profiler.begin("IterativeFibonacciLong"); long iterativeFibLong =// IterativeFibonacci (long) instance
                IterativeFibonacci.fibLong(num);
            Profiler.end("IterativeFibonacciLong");

            System.out.println("(Iterative/long) The " + num + "th Fibonacci "
                    + "number = " + iterativeFibLong + ".");

        }
        }
        }


        System.out.println(); Profiler.print(new PrintWriter(System.out));  //Print out the count, total time, and average time of trials

        System.out.println("... End experiment with different Fibonacci " +
                "implementations");

    }

}
