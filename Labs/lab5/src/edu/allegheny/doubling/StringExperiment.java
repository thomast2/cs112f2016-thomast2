/*
 * Copyright 2014, Michael T. Goodrich, Roberto Tamassia, Michael H. Goldwasser
 *
 * Developed for use with the book:
 *
 *    Data Structures and Algorithms in Java, Sixth Edition
 *    Michael T. Goodrich, Roberto Tamassia, and Michael H. Goldwasser
 *    John Wiley & Sons, 2014
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package edu.allegheny.doubling;

import java.util.Arrays;
/**
 * Provides an empirical test of the efficiency of repeated string concatentation
 * versus use of the StringBuilder class.
 *
 * @author Michael T. Goodrich
 * @author Roberto Tamassia
 * @author Michael H. Goldwasser
 */
public class StringExperiment {

     private static class StatisticsCalculator {
    /* Constructs a new calculateArithmeticMean instance
    * @param long runningTotal
    * @param int
    * @return runningTotal
    **/
    public static long calculateArithmeticMean(Long[] timings, boolean discardFirst) {
      long runningTotal = 0;
      int start = 0;
      if (discardFirst) {
        start = 1;
      }
      for (int i = start; i < timings.length; i++) {
        runningTotal = runningTotal + timings[i];
      }
      return runningTotal / (long)timings.length;
    }
    /* Constructs a new calculateVariance instance
    * @param long arithmeticMean
    * @param long runningTotal
    * @param int start
    * @return runningTotal
    */
   public static long calculateVariance(Long[] timings, boolean discardFirst) {
      long arithmeticMean = calculateArithmeticMean(timings, discardFirst);
      long runningTotal = 0;
      int start = 0;
      if (discardFirst) {
        start = 1;
      }
      for (int i = start; i < timings.length; i++) {
        runningTotal = (timings[i] - arithmeticMean) * (timings[i] - arithmeticMean);
      }
      return runningTotal / (long)timings.length;
    }
    /* Constructs a new calculateStandardDeviation instance
    * @return Math.sqrt
    */
    public static double calculateStandardDeviation(Long[] timings, boolean discardFirst) {
      return Math.sqrt(calculateVariance(timings, discardFirst));
    }

  }


  /** Uses repeated concatenation to compose a String with n copies of character c. */
  public static String repeat1(char c, int n) {
    String answer = "";
    for (int j=0; j < n; j++)
      answer += c;
    return answer;
  }

  /** Uses StringBuilder to compose a String with n copies of character c. */
  public static String repeat2(char c, int n) {
    StringBuilder sb = new StringBuilder();
    for (int j=0; j < n; j++)
      sb.append(c);
    return sb.toString();
  }

  /**
   * Tests the two versions of the 'repeat' algorithm, doubling the
   * size of n each trial, beginning with the given start value. The
   * first command line argument can be used to change the number of
   * trials, and the second to adjust the start value.
   * Gathers timing information for both repeat Algorithms.
   * Calculates standard deviation and arithmetic mean of
   * the gathered timing results according to specified number of trials.
   * @param int n
   * @param int trials
   * @param boolean discardFirst
   * @param Long elapsed1
   * @param Long elapsed2
   * @param int start
   * @param Long startTime2
   * @param Long endTime2
   * @param Long startTime1
   * @param Long endTime1
   */
  public static void main(String[] args) {
    int n = 10000;                           // starting value
    int trials = 7;                      // number of trials
    boolean discardFirst = true;          // discards first timing result when making calculations
    Long[] elapsed1 = new Long[trials];   // array containing repeat1 timing results
    Long[] elapsed2 = new Long[trials];   // array containing repeat2 timing results

    try {
      if (args.length > 0)
        trials = Integer.parseInt(args[0]);
      if (args.length > 1)
        n = Integer.parseInt(args[1]);
    } catch (NumberFormatException e) { }
    int start = n;                                                                       // remember the original starting value

    // Repeat 2 alogrithm using Java StringBuilder class
    System.out.println("Testing repeat2...");
    for (int t=0; t < trials; t++) {
      long startTime2 = System.nanoTime();                                               // Measures time just before the execution of the algorithm
      String temp = repeat2('-', n);                                                     // Instance of repeat2 method
      long endTime2 = System.nanoTime();                                                 // Measures time just after the end of the algorithm
      elapsed2[t] = new Long(endTime2 - startTime2);                                     // Array that contains the difference between startTime and endTime to measure performance of each trial
      System.out.println(String.format("n: %9d took %12d nanoseconds", n, elapsed2[t])); // Prints the time taken for each trial of repeat2
      n *= 2;                                                                            // double the problem size
    }
    // Repeat 1 algorithm using concatenation based on the '+' operator
    System.out.println("Testing repeat1...");
    n = start;                                                                           // restore n to its start value
    for (int t=0; t < trials; t++) {
      long startTime1 = System.nanoTime();                                               // Measures time just before the execution of the algorithm
      String temp = repeat1('-', n);                                                     // Instance of repeat1 method
      long endTime1 = System.nanoTime();                                                 // Measures time just after the end of the algorithm
      elapsed1[t] = new Long(endTime1 - startTime1);                                     // Array that contains the difference between startTime and endTime to measure performance of each trial
      System.out.println(String.format("n: %9d took %12d nanoseconds", n, elapsed1[t])); // Prints the time taken for each trial of repeat1
      n *= 2;                                                                            // double the problem size
    }

    /* Prints the timing results of each trial again, using
    *  the toString method from the java.util.Arrays package.
    *  Prints the standard deviation and arithmetic mean
    *  of each collection of trial data for both repeat1
    *  and repeat2 algorithms
    */
    System.out.println();
    System.out.println("Summary of Timing Results:\n");

    System.out.println("Repeat 2 Time (ns): " + "\n");
    System.out.println(Arrays.toString(elapsed2));
    System.out.println("Arithmetic Mean (ns): "
    + StatisticsCalculator.calculateArithmeticMean(elapsed2, discardFirst));
    System.out.println("Standard Deviation (ns): "
    + StatisticsCalculator.calculateStandardDeviation(elapsed2, discardFirst));
    System.out.println();

    System.out.println("Repeat 1 Time (ns): " + "\n");
    System.out.println(Arrays.toString(elapsed1));
    System.out.println("Arithmetic Mean (ns): "
    + StatisticsCalculator.calculateArithmeticMean(elapsed1, discardFirst));
    System.out.println("Standard Deviation (ns): "
    + StatisticsCalculator.calculateStandardDeviation(elapsed1, discardFirst));
    System.out.println();



  }
}
