import java.util.regex.*;
import java.util.Stack;
import java.util.Scanner;


/**
* The StackMachine program implements an instance of the java.util.Stack class
* which allows the user to insert objects into the stack and decide whether to
* add or swap the top two objects in the stack.
* Also, the program has a stop command which ends the program
* as well as a display command which displays the contents of the stack.
* This program has a minor error checking method which does not allow incorrect
* characters to be an input from the user.
*/

public class StackMachine{

    /**
    * Main method which stores user objects of user input and can add
    * swap or display them given a specific user command.
    * Uses intances of java.util.Stack class and java.util.Scanner class.
    *@param keepGoing
    *@param input
    *@param peek
    *@param first
    *@param second
    *@param firstString
    *@param secondString
    */

    public static void main (String[] args){

    /**
    * Declaration of instance variables scan, machine.
    */
    Scanner scan = new Scanner(System.in);
    Stack<String> machine = new Stack<>();

    /**
    *boolean value determining whether the StackMachine continues.
    */
    boolean keepGoing = true;

    /**
    *Pattern for recognizing characters that are not on the command list.
    */
    String patternString = "[A-Za-z&&[^xdes]]*";
    Pattern p = Pattern.compile(patternString);

    /**
    * User command list.
    */
    System.out.println("This is the stack machine!\n");
    System.out.println("Command\t| Meaning");
    System.out.println("int\t| push int on the stack");
    System.out.println("+\t| push a “+” onto the stack");
    System.out.println("s\t| push an “s” onto the stack");
    System.out.println("d\t| display contents of the stack");
    System.out.println("x\t| stop the stack machine");
    System.out.println("e\t| evaluate the top of the stack (depending on if s(swap) or +(add)\n");
    System.out.println("**Please do not input two consecutive characters before evaluating**");
        /**
        * Loop running through the StackMachine methods
        */
        while (keepGoing == true){
            System.out.print("Please, enter a command from above: "); //Request command from user
            String input = scan.next();                               //Scan in user command
            Matcher m = p.matcher(input);

            machine.push(input);                                      //Push input into stack

            if (m.matches()){                                         //Gives error message if a character inputted is not on the command list.
            machine.pop();
            System.out.println("This is not a command from the command list!");
            }
            if (input.equals("x")){                                   //If input is an x, keepGoing is false so the machine stops
            keepGoing = false;
            }
            if (input.equals("d")){                                   //If input is a d pop off the d at the top of the stack and print the objects
            machine.pop();
            System.out.println("The contents of the stack are as follows:\n");
                if(!machine.empty()){                                 //Iterate through objects in stack, printing them out.
                for (int i = machine.size()-1; i>=0; i--)
                System.out.println(machine.get(i));
            }
            }
            if (input.equals("e")){                                   //If input is an e, pop e off top of the stack and either add top two integers or swap top two integers.
            machine.pop();
                String peek  = machine.peek();                        //Set peek equal to to the top off the stack.

                if (peek.equals("+")){                                //If the top of the stack is a "+" then the top to integers are added together
                machine.pop();                                        //Pop off the plus sign
                int first = Integer.parseInt(machine.pop());          //Set first equal to the integer value of first object in stack
                int second = Integer.parseInt(machine.pop());         //Set second equal to the integer value of the second object in the stack
                int sum  = first+second;                              //Calculate the sum of first and second

                /**
                *Turn the integers back into their respective string
                *and reinsert them into the stack in the correct order.
                *Print the sum.
                */
                String firstString = String.valueOf(first);
                String secondString = String.valueOf(second);
                machine.push(secondString);
                machine.push(firstString);
                System.out.println("The sum of the top two numbers in the stack is: "+sum);
                }

                /**
                *If peek is equal to "s" pop the s off and swap the top two numbers in the stack.
                */
                if (peek.equals("s")){
                    machine.pop();
                    String first = machine.pop();
                    String second = machine.pop();
                    machine.push(first);
                    machine.push(second);
                    System.out.println("The first two objects in the stack were swapped!");
                }

            }
        }
    }
}



