/*
* Adds sentences from text file to an Array.
* Prints the sentences in text file's order.
* Reverses the order and prints sentences in reverse order.
* Imports java Scanner, File, and IOException classes.
*/

import java.util.Scanner;
import java.io.File;
import java.io.IOException;

/*
* Public so it can be accessed by SentencesReverser Class
* @sentences creates new array
* @currentSentence declares new int variable.
* @SENTENCESFILE creates string by reading in the sentences.txt file.
* @NUMBER_OF_SENTENES sets the maximum size of the array.
*/

public class Sentences {

  private Sentence[] sentences;
  private int currentSentence;
  private static final String SENTENCESFILE = "sentences.txt";
  private static final int NUMBER_OF_SENTENCES = 1500;
    /*
    * @sentences creates array with size equal to NUMBER_OF_SENTENCES
    * Initializes currentSentence at 0.
    */
  public Sentences() {
    sentences = new Sentence[NUMBER_OF_SENTENCES];
    currentSentence = 0;
  }

    /*
    * assigns array to variable sentence.
    * currentSentence is set equal to expression so it will not go beyond sentence number from text file.
    */

  public void addSentence(Sentence sentence) {
    sentences[currentSentence] = sentence;
    currentSentence = (currentSentence + 1) % NUMBER_OF_SENTENCES;
  }

    /*
    * Reads in sentences from sentences.txt
    * @fileScanner creates instance of scanner reading from sentences.txt.
    * while loop reading sentences from text file.
    * @sentence new instance of Sentence Class
    * adds string to sentence array.
    */

  public void readSentencesFromFile() throws IOException {
    Scanner fileScanner = new Scanner(new File(SENTENCESFILE));
    while(fileScanner.hasNext()) {
      String sentenceString = fileScanner.nextLine();
      Sentence sentence = new Sentence(sentenceString);
      this.addSentence(sentence);
    }
  }

    /*
    * Prints sentences from sentence array.
    * currentSentence is used in the boolean expression so counter does not go beyond last text line.
    * i is then incremented.
    * Calls getSentence method from Sentences Class.
    */

  public void printSentences() {
    for(int i = 0; i < currentSentence; i++) {
      System.out.println(((Sentence)sentences[i]).getSentence());
    }
  }
    /*
    * Reverses order of sentences by counting the array in reverse.
    * currentSentence - 1 is used in the boolean expression to print last line first.
    * i is then incremented to count down.
    * calls getSentence method from Sentences Class.
    */
  public void reverseSentences() {
      for(int i = currentSentence - 1 ; i >= 0 ; i--){
           System.out.println(((Sentence)sentences[i]).getSentence());
      }

  }

}
