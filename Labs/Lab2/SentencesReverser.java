/*
* Calls methods from Sentences Class.
* Prints methods timing their execution time.
* Imports java util classes, Iterator and Scanner.
* Imports java.io class, IOException.
*/

import java.io.IOException;
import java.util.Scanner;
import java.util.Iterator;

public class SentencesReverser {

/* Main method of the program.
*@scanner creates new instance of the Scanner.
*@sentences creates new instance of Sentences Class.
*@beforeRead and @afterRead measure execution time of sentences.readSentencesFromFile();.
*Same goes for @beforePrint, @afterPrint, @beforeReverse, @afterRevers, respectively.
* Prints sentences from text file, then prints them in their reversed order.
*/

  public static void main(String[] args) throws IOException {
    Scanner scanner = new Scanner(System.in);
    Sentences sentences = new Sentences();

    long beforeRead = System.nanoTime();
    sentences.readSentencesFromFile();
    long afterRead = System.nanoTime();

    long beforePrint = System.nanoTime();
    sentences.printSentences();
    long afterPrint = System.nanoTime();

    long beforeReverse = System.nanoTime();
    sentences.reverseSentences();
    long afterReverse = System.nanoTime();

    sentences.printSentences();


    System.out.println("Read time for sentences.readSentencesFromFile: " + (afterRead - beforeRead));
    System.out.println("Read time for sentences.printSentences: " + (afterPrint - beforePrint));
    System.out.println("Read time for sentences.reverseSentences: " + (afterReverse - beforeReverse));

  }

}
