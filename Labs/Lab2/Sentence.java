/*
* Accessed by Sentences Class
* @id int used to count sentences.
* @nextId counter used for id variable.
* @Sentence String used to save sentences.
*/

public class Sentence {

  private int id;
  private static int nextId = 0;
  private String sentence;

  /*
  * Saves strings in sentence variable.
  */

  public Sentence(String sentence) {
    id = nextId;
    nextId++;
    this.sentence = sentence;
  }
  /*
  * @return returns to id.
  */
  public int getId() {
    return id;
  }
  /*
  * @return returns to sentence;
  */
  public String getSentence() {
    return sentence;
  }
  /*
  * returns new string with news sentences added.
  */
  public String toString() {
    return new String("(" + id + ", " + sentence + ")");
  }
}
