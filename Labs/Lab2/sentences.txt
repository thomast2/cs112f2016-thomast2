The multi-stage fitness test, 
also known as the beep test is a 
series of stages that have
different tasks sometimes
used by sports coaches and
trainers to estimate an athlete's
VO2 max (maximum oxygen uptake).
The most common variation of the
multi-stage fitness test is the
FitnessGram/Cooper PACER test.[1] 
The test is especially useful
for players of sports such as
rugby, football, Australian rules
football, Gaelic football,
hurling, hockey, netball, handball,
tennis, squash, and fitness testing
in schools and colleges plus
many other sports; employed by many
international sporting teams as
an accurate test of cardiovascular
fitness, one of the more important
components of physical fitness.
The test was created in 1982 by 
Luc Léger, University of Montreal[2]
and published in 1983 with a starting
speed of 8 km/h and stages of 2
min duration. The test was
re-published in the European Journal
of Applied Physiology in 1988 in its
present form with a starting speed
of 8.5 km/h and 1 min stages under
the name "The multistage 20 metre
shuttle run test for aerobic fitness".[3] 
Result equivalences between slightly modified versions 
are well explained by Tomkinson et al. in 2003
