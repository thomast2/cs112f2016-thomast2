package edu.allegheny.experiment;

import java.util.Vector;

/**Calculates the time of the Vector, provided by java, to
 * add, set, and remove methods for a user inputted inital size and number of elements.
 * Calls instance of java.util.Vector class.
 * Part of the edu.allegheny.experiment package.
 */

public class VectorBenchmarks {

    //private static int initialListSize = 1000000;
    //private static int addToFrontNumber = 10000;


  /*Main method.
   *Conducts the timing experiment.
   *@param Num
   *@param initialListSize
   *@param addToFrontNumber
   */

    public static void main(String[] args) {
        /*
         * Instance of the java's vector class.
         */
        Vector<Integer> vector = new Vector<Integer>();

        /*
         * Command line arguments taken in by user.
         * Contain the inital size and then the number of elements to work with.
         */
        Integer Num = new Integer(args[0]); int initialListSize = Num.intValue();
        Integer Num2 = new Integer(args[1]); int addToFrontNumber = Num2.intValue();

        /*
         * for loop which creates a Vector vector with initial size
         * according to the user.
         */
        for(int i = 0; i<initialListSize; i++){
            vector.add(i);
        }
        System.out.println("vector size = "+ vector.size());

        /*
         * for loop which times the add method which adds the ith element
         * to the ith position in vector
         */
        long beforeVectorFrontAdd = System.currentTimeMillis();
        for(int i = 0; i<addToFrontNumber; i++){
            vector.add(i, i);
        }
        long afterVectorFrontAdd = System.currentTimeMillis();

        //print statements for add method timing.
        System.out.println("\tVector add method timings: ");
        System.out.println("Vector elapsed time = "+ (afterVectorFrontAdd - beforeVectorFrontAdd) +" milliseconds");
        System.out.println("Vector average add time = "+ (double)(afterVectorFrontAdd - beforeVectorFrontAdd) / (double)addToFrontNumber +" milliseconds");

        /*
         * for loop which times the set method which sets the ith element
         * to i.
         */
        long beforeVectorSet = System.nanoTime();
        for(int i = 0; i<addToFrontNumber; i++){
            vector.set(i, i);
        }
        long afterVectorSet = System.nanoTime();

        //print statements for set method timing.
        System.out.println("\n\tVector set method timings: ");
        System.out.println("Vector elapsed time = "+ (afterVectorSet - beforeVectorSet) +" nanoseconds");
        System.out.println("Vector average set time = "+ (double)(afterVectorSet - beforeVectorSet) / (double)addToFrontNumber +" nanoseconds");

        /*
         * for loop which times the remove method which removes an element
         * at the end of the vector until it gets to the beginning.
         */
        long beforeVectorRemove = System.currentTimeMillis();
        for(int i = addToFrontNumber; i>=0 && i<=addToFrontNumber; i--){
            vector.remove(i);
        }
        long afterVectorRemove = System.currentTimeMillis();

        //print statements for remove method timing.
        System.out.println("\n\tVector remove method timings: ");
        System.out.println("Vector elapsed time = "+ (afterVectorRemove - beforeVectorRemove) +" milliseconds");
        System.out.println("Vector average remove time = "+ (double)(afterVectorRemove - beforeVectorRemove) / (double)addToFrontNumber +" milliseconds");


    }
}
