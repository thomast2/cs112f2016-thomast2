package edu.allegheny.experiment;

import java.util.ArrayList;

/**Calculates the time of the ArrayList, provided by java, to
 * add, set, and remove methods for a user inputted inital size and number of elements.
 * Calls instance of java.util.ArrayList class.
 * Part of the edu.allegheny.experiment package.
 */

public class ArrayListBenchmarks {

  // private static int initialListSize = 1000000;
  //private static int addToFrontNumber = 10000;

  /*Main method.
   *Conducts the timing experiment.
   *@param Num
   *@param initialListSize
   *@param addToFrontNumber
   */

  public static void main(String[] args) {
    /*
     * instance of java's ArrayList class.
     */
    ArrayList<Integer> arrayList = new ArrayList<Integer>();

    /*
     * Command line arguments taken in by user.
     * Contain the inital size and then the number of elements to work with.
     */
    Integer Num = new Integer(args[0]); int initialListSize = Num.intValue();
    Integer Num2 = new Integer(args[1]); int addToFrontNumber = Num2.intValue();

    /*
     * for loop which creates an ArrayList arrayList with initial size
     * according to the user.
     */
    for(int i = 0; i<initialListSize; i++) {
      arrayList.add(i);
    }
    System.out.println("ArrayList size = " + arrayList.size());

    /*
     * for loop which times the add method which adds the ith element
     * to the ith position in arrayList.
     */
    long beforeArrayListFrontAdd = System.currentTimeMillis();
    for(int i = 0; i<addToFrontNumber; i++){
      arrayList.add(i, i);
    }
    long afterArrayListFrontAdd = System.currentTimeMillis();

    //print statements for add method timing
    System.out.println("\n\tArray List add method Timings");
    System.out.println("ArrayList elapsed time = " + (afterArrayListFrontAdd - beforeArrayListFrontAdd) +" milliseconds");
    System.out.println("ArrayList average add time = " + (double)(afterArrayListFrontAdd - beforeArrayListFrontAdd) / (double)addToFrontNumber +" milliseconds");

    /*
     * for loop which times the set method which sets the ith element
     * to i.
     */
    long beforeVectorSet = System.currentTimeMillis();
    for(int i = 0; i<addToFrontNumber; i++){
        arrayList.set(i, i);
    }
    long afterVectorSet = System.currentTimeMillis();

    //print statements for set method timing.
    System.out.println("\n\tArray List set method timings: ");
    System.out.println("Array List elapsed time = "+ (afterVectorSet - beforeVectorSet)+" milliseconds");
    System.out.println("Array List average set time = "+ (double)(afterVectorSet - beforeVectorSet) / (double)addToFrontNumber+" milliseconds");

    /*
     * for loop which times the remove method which removes the ith element
     * starting at the end of the arrayList and finishing at the beginning.
     */
    long beforeVectorRemove = System.currentTimeMillis();
    for(int i = addToFrontNumber; i>=0 && i<=addToFrontNumber; i--){
        arrayList.remove(i);
    }
    long afterVectorRemove = System.currentTimeMillis();
    System.out.println("\n\tArray List remove method timings: ");
    System.out.println("Array List elapsed time = "+ (afterVectorRemove - beforeVectorRemove)+" milliseconds");
    System.out.println("Array List average remove time = "+ (double)(afterVectorRemove - beforeVectorRemove) / (double)addToFrontNumber +" milliseconds");


    }
}


