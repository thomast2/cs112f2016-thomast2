package edu.allegheny.experiment;

import java.util.LinkedList;

/**Calculates the time of the LinkedList, provided by java, to
 * add, set, and remove methods for a user inputted inital size and number of elements.
 * Calls instance of java.util.LinkedList class.
 * Part of the edu.allegheny.experiment package.
 */

public class LinkedListBenchmarks {

  //private static int initialListSize = 1000000;
  //private static int addToFrontNumber = 10000;


  /*Main method.
   *Conducts the timing experiment.
   *@param Num
   *@param initialListSize
   *@param addToFrontNumber
   */

  public static void main(String[] args) {

     /*
     * Command line arguments taken in by user.
     * Contain the inital size and then the number of elements to work with.
     */
   Integer Num = new Integer(args[0]); int initialListSize = Num.intValue();
    Integer Num2 = new Integer(args[1]); int addToFrontNumber = Num2.intValue();

    /*
     * Instance of java's LinkedList class
     */
    LinkedList<Integer> linkedList = new LinkedList<Integer>();

    /*
     * For loop creating linkedList with inital size according to user.
     */
    for(int i = 0; i<initialListSize; i++) {
      linkedList.add(i);
    }
    System.out.println("linkedList size = " + linkedList.size());

    /*
     * for loop which times the add method which adds the ith element to
     * the ith position in linkedList.
     */
    long beforeLinkedListFrontAdd = System.currentTimeMillis();
    for(int i = 0; i<addToFrontNumber; i++){
      linkedList.add(i, i);
    }
    long afterLinkedListFrontAdd = System.currentTimeMillis();

    //print methods for add method timing.
    System.out.println("\n\tLinked List add method timings; ");
    System.out.println("LinkedList elapsed time = " + (afterLinkedListFrontAdd - beforeLinkedListFrontAdd) +" milliseconds");
    System.out.println("LinkedList average add time = " + (double)(afterLinkedListFrontAdd - beforeLinkedListFrontAdd) / (double)addToFrontNumber +" milliseconds");

    /*
     * for loop which times the set method which sets element i to the
     * ith position.
     */
    long beforeVectorSet = System.currentTimeMillis();
    for(int i = 0; i<addToFrontNumber; i++){
            linkedList.set(i, i);
        }
    long afterVectorSet = System.currentTimeMillis();

    //print statements for set method timing.
    System.out.println("\n\tLinked List set method timings: ");
    System.out.println("Linked List elapsed time = "+ (afterVectorSet - beforeVectorSet) +" milliseconds");
    System.out.println("Linked List average set time = "+ (double)(afterVectorSet - beforeVectorSet) / (double)addToFrontNumber +" milliseconds");

    /*
     * for loop which times the remove method which removes an element
     * in linkedList beginning at the end and finishing at the head of
     * the linked list.
     */
    long beforeVectorRemove = System.currentTimeMillis();
    for(int i = addToFrontNumber; i>=0 && i<=addToFrontNumber; i--){
            linkedList.remove(i);
    }
    long afterVectorRemove = System.currentTimeMillis();

    //print statements for remove method timing.
    System.out.println("\n\tLinked List remove method timings: ");
    System.out.println("Linked List elapsed time = "+ (afterVectorRemove - beforeVectorRemove) +" milliseconds");
    System.out.println("Linked List average remove time = "+ (double)(afterVectorRemove - beforeVectorRemove) / (double)addToFrontNumber +" milliseconds");


    }
}



